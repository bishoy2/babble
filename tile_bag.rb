# This class models a bag of Scrabble tiles, according to the 
# standard English Scrabble rules
# Author: Blake Ishoy
class TileBag
	
	# Getter for @tile_set
	attr_accessor :tile_set	
	
	# Creates a TileBag with 98 tiles
	# precondition: none
	# postcondition: a new TileBag with a full 98 tiles is created
	def initialize
		@tile_set = Array.new
		populate_tileset
	end

	# helper for populating the tileset
	def populate_tileset
		12.times do |i|
			@tile_set.push(:E)
		end
		9.times do |i|
			@tile_set.push(:A)
			@tile_set.push(:I)	
		end
		8.times do |i|
			@tile_set.push(:O)	
		end
		6.times do |i|
			@tile_set.push(:N)
			@tile_set.push(:R)
			@tile_set.push(:T)	
		end
		4.times do |i|
			@tile_set.push(:L)
			@tile_set.push(:S)
			@tile_set.push(:U)
			@tile_set.push(:D)	
		end
		3.times do |i|
			@tile_set.push(:G)	
		end
		2.times do |i|
			@tile_set.push(:B)
			@tile_set.push(:C)
			@tile_set.push(:M)
			@tile_set.push(:P)
			@tile_set.push(:F)
			@tile_set.push(:H)
			@tile_set.push(:V)
			@tile_set.push(:W)
			@tile_set.push(:Y)	
		end
		@tile_set.push(:K)
		@tile_set.push(:J)
		@tile_set.push(:X)
		@tile_set.push(:Q)
		@tile_set.push(:Z)
	end

	# Draws a random tile from the TileBag and removes it from the
	# tile_set collection
	# precondition: tile_set.length > 0
	# returns the removed tile
	def draw_tile
		tile = @tile_set.sample
		@tile_set.delete_at(@tile_set.index(tile))
		return tile
	end

	# Gets if the tile_set is empty or not
	# precondition: none
	# postcondition: none
	def empty
		return @tile_set.empty?
	end

	# Static reference method for points per tile
	# precondition: none
	# returns the value of the symbol passed
	def self.points_for(tile)
		tiles_with_points = {
			:A => 	1,
			:B =>	3,
			:C =>	3,
			:D =>	2,
			:E =>	1,
			:F =>	4,
			:G =>	2,
			:H =>	4,
			:I =>	1,
			:J =>	8,
			:K =>	5,
			:L =>	1,
			:M =>	3,
			:N =>	1,
			:O =>	1,
			:P =>	3,
			:Q =>	10,
			:R =>	1,
			:S =>	1,
			:T =>	1,
			:U =>	1,
			:V =>	4,
			:W =>	4,
			:X =>	4,
			:Y =>	4,
			:Z =>	10
		}
		return tiles_with_points[tile]
	end

end





















