#Test class for the TileRack has_tiles_for method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../tile_rack.rb"
class TileRack::TestHasTilesFor < Minitest::Test

	#Tests if the tilerack has enough characters for a given
	#word if the tiles on the rack are in order with no
	#duplicates
	def test_rack_has_needed_letters_when_letters_are_in_order_no_duplicates
		rack = TileRack.new
		rack.append(:A)
		rack.append(:T)
		rack.append(:R)
		rack.append(:U)
		rack.append(:C)
		rack.append(:K)
		rack.append(:B)
		assert_equal true, rack.has_tiles_for?("truck")
	end
	
	#Tests if the tilerack has enough characters for a given
	#word if the tiles on the rack aren't in order with no
	#duplicates
	def test_rack_has_needed_letters_when_letters_are_not_in_order_no_duplicates
		rack = TileRack.new
		rack.append(:A)
		rack.append(:K)
		rack.append(:C)
		rack.append(:U)
		rack.append(:R)
		rack.append(:T)
		rack.append(:B)
		assert_equal true, rack.has_tiles_for?("truck")
	end
	
	#Tests if the tilerack has enough characters for a given
	#word if no needed tiles are on the rack
	def test_rack_doesnt_contain_any_needed_letters
		rack = TileRack.new
		rack.append(:A)
		rack.append(:K)
		rack.append(:C)
		rack.append(:U)
		rack.append(:R)
		rack.append(:T)
		rack.append(:B)
		assert_equal false, rack.has_tiles_for?("door")
	end
	
	#Tests if the tilerack has enough characters for a given
	#word if some needed tiles are on the rack
	def test_rack_contains_some_but_not_all_needed_letters
		rack = TileRack.new
		rack.append(:A)
		rack.append(:K)
		rack.append(:C)
		rack.append(:U)
		rack.append(:R)
		rack.append(:T)
		rack.append(:B)
		assert_equal false, rack.has_tiles_for?("bard")
	end
	
	#Tests if the tilerack has enough characters for a given
	#word with duplicate letters if all needed tiles are on the rack
	def test_rack_contains_a_word_with_duplicate_letters
		rack = TileRack.new
		rack.append(:A)
		rack.append(:G)
		rack.append(:L)
		rack.append(:A)
		rack.append(:S)
		rack.append(:T)
		rack.append(:S)
		assert_equal true, rack.has_tiles_for?("glass")
	end
	
	#Tests if the tilerack has enough characters for a given
	#word with duplicate letters if all needed tiles are on 
	#the rack except for one
	def test_rack_doesnt_contain_enough_duplicate_letters
		rack = TileRack.new
		rack.append(:A)
		rack.append(:G)
		rack.append(:L)
		rack.append(:A)
		rack.append(:X)
		rack.append(:T)
		rack.append(:S)
		assert_equal false, rack.has_tiles_for?("glass")
	end

end
