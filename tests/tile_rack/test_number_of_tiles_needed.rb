#Test class for the TileRack number_of_tiles_needed method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../tile_rack.rb"
class TileRack::TestNumberOfTilesNeeded < Minitest::Test

	#Tests how many tiles an empty rack needs
	def test_empty_tile_rack_should_need_max_tiles
		rack = TileRack.new
		assert_equal 7, rack.number_of_tiles_needed
	end
	
	#Tests how many tiles a rack with one tile needs
	def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles
		rack = TileRack.new
		rack.append(:B)
		assert_equal 6, rack.number_of_tiles_needed
	end
	
	#Tests how many tiles a rack with multiple tiles needs
	def test_tile_rack_with_several_tiles_should_need_some_tiles
		rack = TileRack.new
		rack.append(:B)
		rack.append(:L)
		rack.append(:A)
		rack.append(:K)
		rack.append(:E)
		assert_equal 2, rack.number_of_tiles_needed
	end
	
	#Tests how many tiles a full rack needs
	def test_that_full_tile_rack_doesnt_need_any_tiles
		rack = TileRack.new
		rack.append(:B)
		rack.append(:L)
		rack.append(:A)
		rack.append(:K)
		rack.append(:E)
		rack.append(:A)
		rack.append(:A)
		assert_equal 0, rack.number_of_tiles_needed
	end

end
