#Test class for the TileRack remove_word method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../tile_rack.rb"
class TileRack::TestRemoveWord < Minitest::Test

	#Tests if the tilerack can remove a given
	#word if the tiles on the rack are in order with no
	#duplicates
	def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack
		rack = TileRack.new
		rack.append(:A)
		rack.append(:T)
		rack.append(:R)
		rack.append(:U)
		rack.append(:C)
		rack.append(:K)
		rack.append(:B)
		word = Word.new
		word.append(:T)
		word.append(:R)
		word.append(:U)
		word.append(:C)
		word.append(:K)
		assert_equal word.tiles, rack.remove_word("truck").tiles
	end 
	
	#Tests if the tilerack can remove a given
	#word if the tiles on the rack are not in order with no
	#duplicates
	def test_can_remove_a_word_whose_letters_are_not_in_order_on_the_rack
		rack = TileRack.new
		rack.append(:A)
		rack.append(:K)
		rack.append(:C)
		rack.append(:U)
		rack.append(:R)
		rack.append(:T)
		rack.append(:B)
		word = Word.new
		word.append(:T)
		word.append(:R)
		word.append(:U)
		word.append(:C)
		word.append(:K)
		assert_equal word.tiles, rack.remove_word("truck").tiles
	end 
	
	#Tests if the tilerack can remove a given
	#word with duplicate letters
	def test_can_remove_word_with_duplicate_letters
		rack = TileRack.new
		rack.append(:A)
		rack.append(:G)
		rack.append(:L)
		rack.append(:A)
		rack.append(:S)
		rack.append(:S)
		rack.append(:B)
		word = Word.new
		word.append(:G)
		word.append(:L)
		word.append(:A)
		word.append(:S)
		word.append(:S)
		assert_equal word.tiles, rack.remove_word("glass").tiles
	end 
	
	#Tests if the tilerack can remove a given
	#word with duplicate letters with removing unneeded 
	#duplicates on the rack
	def test_can_remove_word_without_removing_unneeded_duplicate_letters
		rack = TileRack.new
		rack.append(:S)
		rack.append(:G)
		rack.append(:L)
		rack.append(:A)
		rack.append(:S)
		rack.append(:S)
		rack.append(:S)
		word = Word.new
		word.append(:G)
		word.append(:L)
		word.append(:A)
		word.append(:S)
		word.append(:S)
		assert_equal word.tiles, rack.remove_word("glass").tiles
		assert_equal rack.tiles, [:S, :S]
	end 

end
