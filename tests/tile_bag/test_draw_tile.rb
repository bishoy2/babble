# Test class for the draw_tile method in TileBag class
# Author: Blake Ishoy

# filename: test_draw_tile.rb
require "minitest/autorun"
require_relative "../../tile_bag.rb"
class TileBag::TestDrawTile < Minitest::Test
	
	# Tests if the TileBag has the right number of tiles
	def test_has_proper_number_of_tiles
		testbag = TileBag.new
		
		98.times do |i|
			testbag.draw_tile
		end
		
		assert_equal true, testbag.empty
	end
	
	# Tests if the TileBag has the right number of each tile
	def test_has_proper_tile_distribution
		testbag = TileBag.new
		
		assert_equal 12, testbag.tile_set.count(:E)
		assert_equal 9, testbag.tile_set.count(:A)
		assert_equal 9, testbag.tile_set.count(:I)
		assert_equal 8, testbag.tile_set.count(:O)
		assert_equal 6, testbag.tile_set.count(:N)
		assert_equal 6, testbag.tile_set.count(:R)
		assert_equal 6, testbag.tile_set.count(:T)
		assert_equal 4, testbag.tile_set.count(:L)
		assert_equal 4, testbag.tile_set.count(:S)
		assert_equal 4, testbag.tile_set.count(:U)
		assert_equal 4, testbag.tile_set.count(:D)
		assert_equal 3, testbag.tile_set.count(:G)
		assert_equal 2, testbag.tile_set.count(:B)
		assert_equal 2, testbag.tile_set.count(:C)
		assert_equal 2, testbag.tile_set.count(:M)
		assert_equal 2, testbag.tile_set.count(:P)
		assert_equal 2, testbag.tile_set.count(:F)
		assert_equal 2, testbag.tile_set.count(:H)
		assert_equal 2, testbag.tile_set.count(:V)
		assert_equal 2, testbag.tile_set.count(:W)
		assert_equal 2, testbag.tile_set.count(:Y)
		assert_equal 1, testbag.tile_set.count(:K)
		assert_equal 1, testbag.tile_set.count(:J)
		assert_equal 1, testbag.tile_set.count(:X)
		assert_equal 1, testbag.tile_set.count(:Q)
		assert_equal 1, testbag.tile_set.count(:Z)
	end

end
