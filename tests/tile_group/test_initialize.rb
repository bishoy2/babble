#Test class for the TileGroup initialize method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../tile_group.rb"
class TileGroup::TestInitialize < Minitest::Test

	#Tests to see if the TileGroup class
	#can create its tiles array properly
	def test_create_empty_tile_group
		tile_group = TileGroup.new
		assert_equal 0, tile_group.tiles.count
	end

end
