#Test class for the TileGroup hand method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../tile_group.rb"
class TileGroup::TestHand < Minitest::Test

	#Tests the hand if the group is empty
	def test_convert_empty_group_to_string
		tile_group = TileGroup.new
		assert_equal "", tile_group.hand
	end
	
	#Tests the hand if the group has one tile
	def test_convert_single_tile_group_to_string
		tile_group = TileGroup.new
		tile_group.append(:B)
		assert_equal "B", tile_group.hand
	end
	
	#Tests the hand if the group has multiple tiles
	def test_convert_multi_tile_group_to_string
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:L)
		tile_group.append(:A)
		tile_group.append(:K)
		tile_group.append(:E)
		assert_equal "BLAKE", tile_group.hand
	end
	
	#Tests the hand if the group has multiple tiles 
	#with duplicates
	def test_convert_multi_tile_group_with_duplicates_to_string
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:O)
		tile_group.append(:B)
		assert_equal "BOB", tile_group.hand
	end

end
