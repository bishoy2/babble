#Test class for the TileGroup remove method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../tile_group.rb"
class TileGroup::TestRemove < Minitest::Test

	#Tests the removal of the only tile in the group
	def test_remove_only_tile
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.remove(:B)
		assert_equal 0, tile_group.tiles.count
	end
	
	#Tests the removal of the first tile in the group
	def test_remove_first_tile_from_many
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:L)
		tile_group.append(:A)
		tile_group.append(:K)
		tile_group.append(:E)
		tile_group.remove(:B)
		assert_equal :L, tile_group.tiles[0]
		assert_equal :A, tile_group.tiles[1]
		assert_equal :K, tile_group.tiles[2]
		assert_equal :E, tile_group.tiles[3]
	end
	
	#Tests the removal of the last tile in the group
	def test_remove_last_tile_from_many
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:L)
		tile_group.append(:A)
		tile_group.append(:K)
		tile_group.append(:E)
		tile_group.remove(:E)
		assert_equal :B, tile_group.tiles[0]
		assert_equal :L, tile_group.tiles[1]
		assert_equal :A, tile_group.tiles[2]
		assert_equal :K, tile_group.tiles[3]
	end
	
	#Tests the removal of the middle tile in the group
	def test_remove_middle_tile_from_many
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:L)
		tile_group.append(:A)
		tile_group.append(:K)
		tile_group.append(:E)
		tile_group.remove(:A)
		assert_equal :B, tile_group.tiles[0]
		assert_equal :L, tile_group.tiles[1]
		assert_equal :K, tile_group.tiles[2]
		assert_equal :E, tile_group.tiles[3]
	end
	
	#Tests the removal of multiple tiles in the group
	def test_remove_multiple_tiles
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:L)
		tile_group.append(:A)
		tile_group.append(:K)
		tile_group.append(:E)
		
		tile_group.remove(:L)
		tile_group.remove(:A)
		tile_group.remove(:K)
		
		assert_equal :B, tile_group.tiles[0]
		assert_equal :E, tile_group.tiles[1]
	end
	
	#Tests the removal of one tile in the group that has a
	#duplicate of that tile.
	def test_make_sure_duplicates_are_not_removed
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:O)
		tile_group.append(:B)
		
		tile_group.remove(:B)
		
		assert_equal :O, tile_group.tiles[0]
		assert_equal :B, tile_group.tiles[1]
	end

end
