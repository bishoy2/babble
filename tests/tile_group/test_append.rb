#Test class for the TileGroup append method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../tile_group.rb"
class TileGroup::TestAppend < Minitest::Test
	
	#Tests the appending of one tile
	def test_append_one_tile
		tile_group = TileGroup.new
		tile_group.append(:B)
		assert_equal :B, tile_group.tiles[0]
	end
	
	#Tests the appending of multiple tiles
	def test_append_many_tiles
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:L)
		tile_group.append(:A)
		tile_group.append(:K)
		tile_group.append(:E)
		assert_equal :B, tile_group.tiles[0]
		assert_equal :L, tile_group.tiles[1]
		assert_equal :A, tile_group.tiles[2]
		assert_equal :K, tile_group.tiles[3]
		assert_equal :E, tile_group.tiles[4]
	end
	
	#Tests the appending of duplicate tiles
	def test_append_duplicate_tiles
		tile_group = TileGroup.new
		tile_group.append(:B)
		tile_group.append(:B)
		tile_group.append(:B)
		assert_equal :B, tile_group.tiles[0]
		assert_equal :B, tile_group.tiles[1]
		assert_equal :B, tile_group.tiles[2]
	end

end
