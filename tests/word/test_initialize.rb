#Test class for the Word initialize method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../word.rb"
class Word::TestInitialize < Minitest::Test

	#Tests that the constructor works
	def test_create_empty_word
		word = Word.new
		assert_equal 0, word.tiles.count
	end	

end
