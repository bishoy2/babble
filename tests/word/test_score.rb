#Test class for the Word score method
#Author: Blake Ishoy
require "minitest/autorun"
require_relative "../../word.rb"
class Word::TestScore < Minitest::Test
	
	#Tests an empty word, which should have a
	#score of zero.
	def test_empty_word_should_have_score_of_zero
		word = Word.new
		assert_equal 0, word.score
	end
	
	#Tests a one tile word
	def test_score_a_one_tile_word
		word = Word.new
		word.append(:A)
		assert_equal 1, word.score
	end
	
	#Tests a multiple tile word
	def test_score_a_word_with_multiple_different_tiles
		word = Word.new
		word.append(:B)
		word.append(:L)
		word.append(:A)
		word.append(:K)
		word.append(:E)
		assert_equal 11, word.score
	end
	
	#Tests a multiple tile word with recurring letters
	def test_score_a_word_with_recurring_tiles
		word = Word.new
		word.append(:D)
		word.append(:E)
		word.append(:V)
		word.append(:E)
		word.append(:L)
		word.append(:O)
		word.append(:P)
		word.append(:M)
		word.append(:E)
		word.append(:N)
		word.append(:T)
		assert_equal 19, word.score
	end
	
end
