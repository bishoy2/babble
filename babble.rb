require 'spellchecker'
require 'tempfile'
require_relative 'tile_bag.rb'
require_relative 'tile_rack.rb'
require_relative 'tile_group.rb'
require_relative 'word.rb'
#Driver class for Babble, runs the app.
#Author: Blake Ishoy
class Babble
	
	#constructor for Babble
	def initialize
		@tile_bag = TileBag.new()
		@tile_rack = TileRack.new()
		@word = Word.new()
		@current_score = 0
	end
	
	#Main game loop, directs game logic
	def run
		game_is_over = false
		until game_is_over do
			refresh_hand
			
			input = get_player_input
			
			is_a_word = Spellchecker::check(input)[0][:correct]
			only_contains_letters_from_rack = @tile_rack.has_tiles_for?(input)
			
			if only_contains_letters_from_rack && is_a_word
				@word = @tile_rack.remove_word(input)
				score = @word.score
				puts "You made " + input + " for " + score.to_s + " points."
				@current_score += score
			elsif !is_a_word
				puts "Not a valid word"
			elsif !only_contains_letters_from_rack
				puts "Not enough tiles"
			end
			
			puts "Your current score is: " + @current_score.to_s

		end
	end
	
	#Refreshes player's hand by loading in new tiles to replace
	#the old ones.
	def refresh_hand
		tiles_needed = @tile_rack.number_of_tiles_needed
		tiles_needed.times do |i|
			@tile_rack.append(@tile_bag.draw_tile)
		end
	end
	
	#Gets player input word, also handles exit functionality.
	def get_player_input
		puts "Type a word formed from the letters you have"
		puts "Or type 'quit' to exit the application"
		puts "Your current tiles: " + @tile_rack.hand
		input = gets.chomp
		if input == :quit.to_s
			puts "Thanks for playing, total score: " + @current_score.to_s
			exit
		end
		return input
	end

end

Babble.new.run
