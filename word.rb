#This class models a group of tiles that the player
#has decided form a word.
#Author: Blake Ishoy
class Word < TileGroup
	
	#Constructor for the Word class
	def initialize
		super
	end
	
	#Sums up the points of all the tiles contained
	#in the word
	def score
		total = 0
		@tiles.each do |i|
			total += TileBag.points_for(i)
		end
		return total
	end

end 
