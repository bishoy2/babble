#This class models the group of tiles 
#held by a player throughout the game.
#Author: Blake Ishoy
class TileGroup
	
	#getter for tiles Array
	attr_accessor :tiles
	
	#Constructor for TileGroup, initializes tiles
	def initialize
		@tiles = Array.new
	end
	
	#Appends a tile onto the group
	def append(tile)
		@tiles.push(tile)
	end
	
	#Removes the tile from the group
	def remove(tile)
		@tiles.delete_at(@tiles.index(tile))
	end
	
	#Returns the concatenation of all the tiles held by the player.
	def hand
		return @tiles.join("")
	end
	
end
