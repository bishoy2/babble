#This class models the player's current tilerack.
#Author: Blake Ishoy
require_relative 'tile_group.rb'
class TileRack < TileGroup
	
	# Constructor for TileRack class
	def initialize
		super
	end
	
	#Determines the number of tiles needed to 
	#file the rack back up.
	def number_of_tiles_needed
		count = @tiles.count
		return 7 - count
	end
	
	#Returns whether the rack has the correct amount of tiles
	#to create the input word
	def has_tiles_for?(text)
		text = text.upcase
		return_value = false
		characters = text.split(//)
		temp_rack = @tiles.join(" ").split
		building_word = ""
		
		characters.each do |i|
			if temp_rack.include?(i)
				building_word += i
				temp_rack.delete_at(temp_rack.index(i))
			end
		end
		
		if building_word == text
			return_value = true
		end
		
		return return_value
	end
	
	#Removes the word from the tile rack
	def remove_word(text)
		word = Word.new()
		text = text.upcase
		characters = text.split(//)
		
		characters.each do |i|
			word.append(i.to_sym)
			remove(i.to_sym)
		end
		
		
		
		return word
	end
	
end
